﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinationScript : MonoBehaviour {


    private LevelManager LevelManager;
    private PlayerScript playerScript;

  public int ID;

    // Use this for initialization
    void Awake()
    {

        LevelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();


    }

    // Update is called once per frame
    void Update()
    {


    }


    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.tag == "Taxi")
        {
            playerScript = target.gameObject.GetComponent<PlayerScript>();

            if (playerScript.passangerCollected == ID)
            { 
            LevelManager.PassangerSpawner();
            playerScript.passangerCollected = 0;
            playerScript.score += 1;
            Destroy(this.gameObject);
            }

        }



    }

}
