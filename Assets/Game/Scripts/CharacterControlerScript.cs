﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControlerScript : MonoBehaviour {

  public float speed;
  public float turnspeed;
  public float rotation;
  public float acceleration;
  public float friction;
  public float maxVelocity;
  public bool useController;

  Rigidbody2D rbody;
  Transform playerTransform;
   
  // Use this for initialization
  void Start() {
    rbody = GetComponent<Rigidbody2D>();
    playerTransform = this.gameObject.transform;
  }
    // Update is called once per frame
  void FixedUpdate() {
    Vector3 playerPos = playerTransform.eulerAngles;
    if (!useController) {
      // Rotate to the right or left
      if (Input.GetKey (KeyCode.LeftArrow)) {
        transform.Rotate (0, 0, 1 * turnspeed * Time.deltaTime);
      }
      if (Input.GetKey (KeyCode.RightArrow)) {
        transform.Rotate (0, 0, -1 * turnspeed * Time.deltaTime);
      }
      if (Input.GetKey (KeyCode.UpArrow)) {
        if (Mathf.Abs (transform.InverseTransformVector (rbody.velocity).y) < maxVelocity) { 
          rbody.AddForce (transform.up * speed, ForceMode2D.Impulse);
          rbody.drag = friction;
          rbody.angularDrag = friction;
        }
      }
      if (Input.GetKey (KeyCode.DownArrow)) {
        if (Mathf.Abs (transform.InverseTransformVector (rbody.velocity).y) < maxVelocity) {
          rbody.AddForce (transform.up * -speed, ForceMode2D.Impulse);
          rbody.drag = friction;
          rbody.angularDrag = friction;
        }
      } 

    } else {
      transform.Rotate (0, 0, -rotation * turnspeed * Time.deltaTime);
      if (Mathf.Abs (transform.InverseTransformVector (rbody.velocity).y) < maxVelocity) { 
        rbody.AddForce (acceleration * transform.up * speed, ForceMode2D.Impulse);
        rbody.drag = friction;
        rbody.angularDrag = friction;
      }
    }
  }
}



