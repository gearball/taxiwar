﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;
using ZXing;
using ZXing.QrCode;
using System;

public class GameClient : MonoBehaviour {

  [SerializeField] Text debugText;

  WebCamTexture camTexture;
  [SerializeField] RawImage camView;

  NetworkClient client;

  bool isConnected;
  bool stopChecking;

  int playerNumber;

  [SerializeField] AudioSource engine;

	// Use this for initialization
	void Start () {
    joystickBase.gameObject.SetActive (false);
    camTexture = new WebCamTexture (480, 320);
    camTexture.Play ();
    camView.texture = camTexture;
	}
	
	// Update is called once per frame
	void Update () {
    if (Input.GetKeyUp (KeyCode.Escape)) {
      Application.Quit ();
    }
    if (!isConnected) {
      CheckQRCode ();
    } else {
      CheckJoystick ();
    }
	}

  public void Launch (string ipAddress, int port) {
    client = new NetworkClient ();
    client.RegisterHandler (MsgType.Connect, OnConnected);
    client.RegisterHandler (MsgType.Error, OnError);
    client.RegisterHandler (MsgType.Disconnect, OnDisconnected);
    client.RegisterHandler (RegisterMessage.ID, OnRegisterMessage);
    client.Connect(ipAddress, port);
    debugText.text = string.Format ("Initiate connection to {0}:{1}", ipAddress, port);
  }

  void OnRegisterMessage (NetworkMessage msg) {
    RegisterMessage register = msg.ReadMessage<RegisterMessage> ();
    playerNumber = register.playerNumber;
    debugText.text = string.Format ("Player #{0}", register.playerNumber);
  }

  void OnConnected (NetworkMessage msg) {
    isConnected = true;
    debugText.text = "Connected To Server!";
  }

  void OnDisconnected (NetworkMessage msg) {
    isConnected = false;
    debugText.text = "Disconnected from Server!";
    stopChecking = false;
    camView.gameObject.SetActive (true);
  }

  void OnError (NetworkMessage msg) {
    debugText.text = "Error happened: " + msg.msgType;
    stopChecking = false;
    camView.gameObject.SetActive (true);
  }

  bool hasLeftTouch;
  Touch leftTouch;

  [SerializeField] RectTransform joystickKnob;
  [SerializeField] RectTransform joystickBase;
  [SerializeField] float maxRadius;
  [SerializeField] float currentRadius;

  Vector2 _movement;
  Vector2 movement {
    get { 
      return _movement;
    }
    set {
      MovementMessage movement = new MovementMessage ();
      movement.x = value.x;
      movement.y = value.y;
      if (isConnected) {
        client.Send (MovementMessage.ID, movement);
      }
    }
  }

  public void SendJoypad (int index, JoypadState state) {
    JoypadMessage joypad = new JoypadMessage ();
    joypad.index = index;
    joypad.state = state;
    if (index == 3) {
      if (state == JoypadState.Down) {
        engine.Play ();
      } else {
        engine.Stop ();
      }
    }
    if (isConnected) {
      client.Send (JoypadMessage.ID, joypad);
    }
  }

  void CheckJoystick () {
    for (int i = 0; i < Input.touches.Length; i++) {
      Touch touch = Input.touches[i];
      if (!hasLeftTouch && touch.phase == TouchPhase.Began && touch.position.x <= Screen.width / 2) {
        hasLeftTouch = true;
        leftTouch = touch;
        joystickBase.gameObject.SetActive (true);
        joystickBase.position = touch.position;
      }

      if (hasLeftTouch && touch.phase == TouchPhase.Moved && touch.fingerId == leftTouch.fingerId) {
        Vector2 position = touch.position;
        if (Vector2.Distance (joystickBase.position, position) > maxRadius) {
          position = Vector2.MoveTowards (joystickBase.position, position, maxRadius);
        }
        currentRadius = Vector2.Distance (joystickBase.position, position);
        joystickKnob.position = position;
        movement = joystickKnob.localPosition / joystickBase.InverseTransformPoint(joystickBase.position + (Vector3.right * maxRadius)).x;
      }

      if (hasLeftTouch && touch.phase == TouchPhase.Ended && touch.fingerId == leftTouch.fingerId) {
        hasLeftTouch = false;
        joystickBase.gameObject.SetActive (false);
        movement = Vector2.zero;
      }
    }
  }

  void CheckQRCode () {
    try {
      IBarcodeReader barcodeReader = new BarcodeReader ();
      var result = barcodeReader.Decode (camTexture.GetPixels32 (), camTexture.width, camTexture.height);
      if (result != null) {
        stopChecking = true;
        camView.gameObject.SetActive (false);
        Debug.Log (result.Text);
        string[] info = result.Text.Split (new string[] { ":" }, StringSplitOptions.None);
        Launch (info[0], int.Parse (info[1]));
      }
    } catch (Exception e) {
      Debug.Log (e.Message);
    }
  }
}
