﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelManager : MonoBehaviour {

    public GameServer gameServer;
    public GameObject[] passangers;    // Where the types of passangers are kept
    public GameObject[] destinations;   // where the possible destinations are;
    public TextMeshProUGUI timer;
    public float maxTime  = 180f;
    private int passangerSpawnCount;   // Which passanger did i spawn
    private int destinationSpawnCount; //  Which Destination did i just spawn.
    private int NoPlayers;   // Number of people playing 

  int passengerCounter = 1;

    // Use this for initialization
    void Awake ()
    {
        passangerSpawnCount = 0;
        destinationSpawnCount = 0;
        NoPlayers = 2;
        PassangerSpawner();
        PassangerSpawner();




    }
	
	// Update is called once per frame
	void Update ()
    {
        // Debug.Log(passangerSpawnCount + "PassangerSpawnCount");
        // Debug.Log(destinationSpawnCount + "DestinationSpawnCount");
        maxTime = maxTime - Time.deltaTime;
        timer.text = maxTime.ToString("F2");       
        if (maxTime >= 0)
        {
          if ( gameServer != null)
            {
                gameServer.Restart();
            }

        }

    }


    public void PassangerSpawner()

    {
        Vector3 passangerPosition = new Vector3(0, 0, 0);
        Quaternion passangerRotation = Quaternion.Euler(0, 0, 0);

        if (passangerSpawnCount >= 6)
        {
            passangerSpawnCount = 0;
        }
        int i = passangerSpawnCount;



        if (NoPlayers <= 3)
        {

         passangerPosition.x = passangers[i].transform.position.x;
         passangerPosition.y = passangers[i].transform.position.y;
      GameObject p = Instantiate(passangers[i], passangerPosition, passangerRotation);
      passangerSpawnCount += 1;
      p.GetComponent<PassangerScript> ().ID = passengerCounter;
      passengerCounter++;


        }
        else if ( NoPlayers > 3)
        {
         passangerPosition.x = passangers[i].transform.position.x;
         passangerPosition.y = passangers[i].transform.position.y;
         GameObject p = Instantiate(passangers[i], passangerPosition, passangerRotation);
         passangerSpawnCount += 1;
      p.GetComponent<PassangerScript> ().ID = passengerCounter;
      passengerCounter++;



         passangerPosition.x = passangers[i].transform.position.x;
         passangerPosition.y = passangers[i].transform.position.y;
      GameObject q = Instantiate(passangers[i], passangerPosition, passangerRotation);
      passangerSpawnCount += 1;
      q.GetComponent<PassangerScript> ().ID = passengerCounter;
      passengerCounter++;

        }

        
    }

  public  void  DestinationSpawner (int id)
    {
        Vector3 destinationPosition = new Vector3(0, 0, 0);
        Quaternion destinationRotation = Quaternion.Euler(0, 0, 0);

        if (destinationSpawnCount >= 8)
        {
            destinationSpawnCount = 0;
        }
        int i = destinationSpawnCount;


        destinationPosition.x = destinations[i].transform.position.x;
        destinationPosition.y = destinations[i].transform.position.y;
        GameObject d = Instantiate(destinations[i], destinationPosition, destinationRotation);
    d.GetComponent<DestinationScript> ().ID = id;
        destinationSpawnCount += 1;

    }

    void Playerspawner() {

    }

    
}
