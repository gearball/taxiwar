﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Joypad : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

  [SerializeField] GameClient gameClient;
  [SerializeField] int index;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

  public void OnPointerDown (PointerEventData data) {
    gameClient.SendJoypad (index, JoypadState.Down);
  }

  public void OnPointerUp (PointerEventData data) {
    gameClient.SendJoypad (index, JoypadState.Up);
  }
}
