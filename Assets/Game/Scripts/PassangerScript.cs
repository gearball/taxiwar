﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassangerScript : MonoBehaviour {


    private LevelManager LevelManager;
  private PlayerScript playerScript;

  public int ID;

    // Use this for initialization
    void Awake ()
    {

        LevelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();



    }
	
	// Update is called once per frame
	void Update () {

		
	}


    private void OnTriggerEnter2D (Collider2D target)
    {
      if(target.tag == "Taxi")
      {

      playerScript = target.gameObject.GetComponent<PlayerScript> ();
      if (playerScript.passangerCollected == 0) {
        playerScript.passangerCollected = ID;
        LevelManager.DestinationSpawner (ID);
        Destroy (this.gameObject);
      }

      }
    


    }
    
}
