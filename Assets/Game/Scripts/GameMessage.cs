﻿using UnityEngine;
using UnityEngine.Networking;

public class GameMessage : MessageBase {

  public const short Type = MsgType.Highest + 1000;

}

public class MovementMessage : MessageBase {
  public const short ID = MsgType.Highest + 1001;
  public float x;
  public float y;
}

public enum JoypadState {
  Down,
  Up
}

public class JoypadMessage : MessageBase {
  public const short ID = MsgType.Highest + 1002;
  public JoypadState state;
  public int index;
}

public class RegisterMessage : MessageBase {
  public const short ID = MsgType.Highest + 1003;
  public int playerNumber;
}

