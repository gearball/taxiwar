﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class BuildController : MonoBehaviour {

  public void GoServer () {
    SceneManager.LoadScene ("Server");
  }

  public void GoClient () {
    SceneManager.LoadScene ("Client");
  }

  void Update () {
    if (Input.GetKeyUp (KeyCode.Escape)) {
      Application.Quit ();
    }
  }

}
