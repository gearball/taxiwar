﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Linq;
using ZXing;
using ZXing.QrCode;

public class GameServer : MonoBehaviour {

  [SerializeField] Text debugText;
  [SerializeField] int port;

  public class Player {
    public NetworkConnection conn;
    public CharacterControlerScript taxi;
    public float timeout;
    public int number;
    public int acceleration;
  }

  Dictionary<string, Player> players;
  int currentNumber = 1;

  [SerializeField] float maxTimeout;
  [SerializeField] CharacterControlerScript taxiPrefab;
  [SerializeField] RawImage qrAddress;
  Texture2D qrTexture;
  public static GameServer instance;


    // Use this for initialization
    void Start () {
    players = new Dictionary<string, Player> ();
    Launch ();
    debugText.text = Network.player.ipAddress;
    qrAddress.texture = generateQR (Network.player.ipAddress + ":" + port);

        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
    if (Input.GetKeyUp (KeyCode.Escape)) {
      Application.Quit ();
    }
    Dictionary<string,Player>.KeyCollection keys = players.Keys;
    float time = Time.realtimeSinceStartup;
    foreach (string key in keys) {
      Player player = players[key];
      if (player.acceleration > 0) {
        player.taxi.acceleration = 1;
      } else if (player.acceleration < 0) {
        player.taxi.acceleration = -1;
      }
      if (time - players[key].timeout > maxTimeout) {
        DisconnectUser (key);
      }
    }
  }

    public void Restart()
    {

        Dictionary<string, Player>.KeyCollection keys = players.Keys;
        foreach (string key in keys)
        {
                DisconnectUser(key);
        }

    }

  public void DisconnectUser (string key) {
    if (players.ContainsKey (key)) {
      Player player = players[key];
      player.conn.Disconnect ();
      Destroy (player.taxi.gameObject);
      players.Remove (key);
    }
  }

  public void Launch () {
    NetworkServer.Reset ();
    NetworkServer.RegisterHandler (MsgType.Error, OnError);
    NetworkServer.RegisterHandler (MsgType.Connect, OnUserConnected);
    NetworkServer.RegisterHandler (MovementMessage.ID, OnMovementMessage);
    NetworkServer.RegisterHandler (JoypadMessage.ID, OnJoypadMessage);
    NetworkServer.Listen (port);
  }

  void OnError (NetworkMessage msg) {
    debugText.text = "Error happened: " + msg.msgType;
  }

  void OnMovementMessage (NetworkMessage msg) {
    MovementMessage movement = msg.ReadMessage<MovementMessage> ();
    Player player = players[msg.conn.address];
    player.taxi.rotation = movement.x;
    debugText.text = string.Format (string.Format("[{0}] x:{1} | y:{2}", msg.conn.address, movement.x, movement.y));
    player.timeout = Time.realtimeSinceStartup;
  }

  void OnJoypadMessage (NetworkMessage msg) {
    JoypadMessage joypad = msg.ReadMessage<JoypadMessage> ();
    Player player = players[msg.conn.address];
    if (joypad.index == 3) {
      if (joypad.state == JoypadState.Down) {
        player.acceleration = 1;
      } else {
        player.acceleration = 0;
        player.taxi.acceleration = 0;
      }
    } else if (joypad.index == 2) {
      if (joypad.state == JoypadState.Down) {
        player.acceleration = -1;
      } else {
        player.acceleration = 0;
        player.taxi.acceleration = 0;
      }
    }
    debugText.text = string.Format (string.Format("[{0}] Joypad {1} is {2}", msg.conn.address, joypad.index, joypad.state));
    player.timeout = Time.realtimeSinceStartup;
  }

  void OnUserConnected (NetworkMessage msg) {
    Debug.Log (msg.conn.address);
    if (!players.ContainsKey (msg.conn.address)) {
      Player player = new Player ();
      player.timeout = Time.realtimeSinceStartup;
      player.number = currentNumber;
      currentNumber++;
      players.Add (msg.conn.address, player);
      debugText.text = "new player joins " + msg.conn.address;
      CharacterControlerScript taxi = Instantiate<CharacterControlerScript> (taxiPrefab);
      taxi.useController = true;
      player.conn = msg.conn;
      player.taxi = taxi;
    }
    RegisterMessage register = new RegisterMessage ();
    register.playerNumber = players[msg.conn.address].number;
    players[msg.conn.address].conn.Send (RegisterMessage.ID, register);
  }

  Color32[] Encode(string textForEncoding, int width, int height) {
    var writer = new BarcodeWriter {
      Format = BarcodeFormat.QR_CODE,
      Options = new QrCodeEncodingOptions {
        Height = height,
        Width = width
      }
    };
    return writer.Write(textForEncoding);
  }

  Texture2D generateQR(string text) {
    var encoded = new Texture2D (256, 256);
    var color32 = Encode(text, encoded.width, encoded.height);
    encoded.SetPixels32(color32);
    encoded.Apply();
    return encoded;
  }
}
